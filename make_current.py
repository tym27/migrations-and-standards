#!/usr/bin/env python3
# flake8: noqa: E501
"""
Dump a database and split the output into a directory of SQL scripts,
one per object.

Each script is named after the object whose SQL statements it contains.
"""

import argparse
import os
import re
import sys
from urllib.parse import urlparse

import sh
from dotenv import load_dotenv


# Set up regex vars for splitting the main dump file.
SPLIT_RE = re.compile(r'''

--
-- ''')

IDENTIFY_RE = re.compile(r'''

--
-- Name: ([^\n;]+?)(?:\([^)]*\))?; Type: ([^\n;]+); Schema: ([^\n;]+?); Owner:[^\n]*
--

''', re.DOTALL | re.MULTILINE)


def dump_database(target_directory, dburl):
    # ensure we have a target for the dump
    if not os.path.isdir(target_directory):
        os.mkdir(target_directory)
    sqlpath = os.path.join(target_directory, '_schema-dump.sql')
    with open(sqlpath, 'w') as dumpfile:
        sh.pg_dump('-Osx', dburl, _out=dumpfile)


def split_dumpfile(target_directory):
    sqlpath = os.path.join(target_directory, '_schema-dump.sql')
    with open(sqlpath, 'r') as sqlfile:
        sql = sqlfile.read()

    # clear the old SQL files, so dropped objects don't stick around
    for file_ in os.listdir(target_directory):
        if '.sql' in file_ and file_ != '_schema-dump.sql':
            sh.rm(os.path.join(target_directory, file_))

    parts = [match.start() for match in SPLIT_RE.finditer(sql)]
    for start, end in zip(parts[:-1], parts[1:]):
        part = sql[start:end]
        match = IDENTIFY_RE.match(part)
        if not match:
            print(part)
            sys.exit(0)
        name = match.group(1).replace(' ', '_')
        type_ = match.group(2).replace(' ', '_')
        type_ = type_.lower()
        schema = match.group(3)
        if schema == '-':
            schema = 'no_schema'
        if name == 'id':
            continue
        dump_filename = '{}-{}.sql'.format(type_, name)
        dump_filepath = os.path.join(target_directory, dump_filename)
        try:
            with open(dump_filepath, 'x') as dumpfile:
                dumpfile.write(part)
        except FileExistsError:
            pass

def main():
    envpath = os.path.join(os.getcwd(), '.env')
    load_dotenv(envpath, override=True)

    dburl = os.getenv(
        'DATABASE_URL',
        'postgres://collect@104.198.188.123:5433/warehouse'
        )

    target_directory = os.path.join(os.getcwd(), 'current')
    dump_database(target_directory, dburl)
    split_dumpfile(target_directory)



if __name__ == '__main__':
    main()
