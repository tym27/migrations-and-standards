# Data Architecture: Physical data modeling standards

Submitted by Thomas Yager-Madden, Data Architect 

## Overview

This document provides a set of naming and other standards that apply to physical data models. A physical data model represents the perspective of data management technicians, and should follow standards wherever practicable, to preserve consistency and to help achieve efficient use and reuse of data. The primary aim of any such model is to provide a detailed specification for the physical implementation of a database. A physical model is typically generated or derived from a prior logical model -- but for small or prototype projects, there are cases in which beginning directly with a physical model will be appropriate.

To aid rapid application development, Data Architecture requests to take part in the development process from the earliest stages of any project. This allows suggestions in the use of existing structures, incorporating database standards in the initial design, and eliminating unnecessary rework to retrofit standards.

## Principles

- All data models should be as self-documenting as practicable.

  - Metadata should be accessible with same tools used to access data
  - When selecting terms and tokens in database object names, give preference to technical precision (i.e., consistency with application usage, other database systems, etc.) over business usage.

- Transaction-Processing (OLTP) databases should be optimized for atomic `insert`, `update`, and `delete` operations, with consideration given to database reading needs of the client application(s), as well.

  - Typically, keep indexes to a minimum
  - Consider tactical denormalization where it simplifies or speeds common `select` operations.

- Analytic (OLAP) databases should be optimized for large aggregate `select` queries over large sets and ranges of historical data

  - Index aggressively to speed large sorting operations and join processing
  - Pre-calculate commonly used aggregations

- Although these standards are more strict than the logical modeling conventions, they are meant to aid pragmatic modeling, not to get in the way. To this extent they may be somewhat flexible where the situation warrants it -- common sense can go a long way in such circumstances. In particular, if a certain ORM or other application development framework assumes different standards, it is often better to allow those to stand rather than have to add a lot of code to override the customary expectations.

## Common naming standards

- Most physical database object names can be set directly or deterministically from the logical model where there is one.
- The default maximum number of characters in PostgreSQL identifiers (i.e., database object names) is 63 bytes. This is usually enough for all class and property terms to be spelled out fully in names.

  - For any long word that is used in multiple names, if it becomes necessary to abbreviate it to fit within the limit, it should be abbreviated the same way wherever it occurs. Any such abbreviations should be added to this standard as it is introduced.
  - To help with brevity, standard representation terms from the [Logical modeling conventions](./data-modeling-logical-conventions.md) have standard abbreviations (see list below).
  - Keep in mind table names become part of index and constraint names, _which also have to meet the 63-byte limit. Shorter is still better_, although clarity should have priority.

- PostgreSQL is not case-insensitive precisely, but by default upper- or mixed case names are converted to lowercase internally. The exception is for double-quoted names, but those introduce an amount of error-prone-ness and risk. For simplicity and consistency, all names for the physical model should be all lowercase and use underscores for whitespace.

## Database and schema names

- Warehouse databases should be named for their contents, not according to client applications, project names, or other usage information.
- Data mart database names include the prefix `data_mart_`
- Master data database names use the suffix `_master`
- The Postgres default `public` schema should not be used. Create at least one other schema, or more if the data has some logical divisions, named again for the contents. The `search_path` for the database should be set to include your created schema(s) and not include `public`.
- Interactive OLTP databases created primarily to serve to a particular client application may take their name from the application they serve to.

## Table names

- Although we can isolate physical data by database and schema or namespace, even so it simplifies queries when table names are unique across  database systems.
- Because a number of databases will need similar tables such as `user`, `client`, or `line_item` etc, a short prefix can be used in table names (usually an abbreviation of the schema name) to preserve uniqueness. Where this is done, it should be applied to every table in the schema.
- Reference or "lookup" data tables should have the prefix `ref_`.
- Tables used by ETL for interim processing steps should have the prefix `intm_`.
- Dimension tables in data marts should have the prefix `dim_`
- Dimension table names should have a topic or class name and an indication of the lowest level in the hierarchy.
- Fact tables have the prefix `fact_`
- Fact table names should indicate the subject matter of the metrics, and the key dimension(s) and level at which they are stored.

  - For the sake of brevity, do not attempt to include an exhaustive list of dimensions in the fact table name. Just include dimension information as needed to distinguish from other sets of facts with similar metrics.
  - E.g. `fact_delivery_by_line_item`, `fact_forecast_by_month`, etc.

- Dim and Fact table prefixes come after any schema-abbreviation prefix, if used.
- Any experimental or provisional tables created by users to try something out, obtain a dataset for a particular one-off request, etc. should have the prefix `z_`. These are liable to be dropped by Database Administrators at any time and without warning. If a `z_` table becomes part of a process that has to be performed more than twice, the table should be moved to its most appropriate database and schema, and given a new name without the prefix that fits the rest of this standard.

### Identifiers

- Every table should have a serial integer or bigint as a surrogate primary key.

  - Many-to-many resolution tables can have a compound primary key using the two parent foreign keys _as long as the association table has no additional attributes of its own_, in which case a single surrogate key should be created as with any other table.

- Primary key columns should have the suffix `_id` and be declared as `serial` or `bigserial` data type. No other columns should be named `_id`.
- Primary key names should begin with the name of the table, without any prefixes. This will often allow for the use of natural join syntax when the table has foreign keys in other tables.
- Values that are imported from outside systems (via ETL, etc.) having `_id` in the name should be changed to `_cd` (code). This helps distinguish identifiers under our direct control from ones we have imported. Where practical, the source of the code should be indicated in the column name (e.g., `dfp_network_code`)

## Column names

- As with table names, column names will generally be created from the names used in the logical model, with representation terms (id, count, code, etc) abbreviated in accordance with the abbreviations section of this standard.
- To ease query writing and legibility, effort should be made to keep column names unique across the database. Usually prefixing with all or part of the table name (without its prefix) is a good way to make unique column names for common elements such as `id`, `name`, `description`, etc.
- Many column names apply to attributes that other entities in the model will not have. In these cases, the table name prefix is not needed.
- Record-keeping or ETL "audit" columns such as `created_dt` are not directly used in queries as often, and should be repeated the same way in every table that has them. (This also aids in creating standard trigger code in the database to help with populating these columns).

### Exception for raw data hold tables

- Rather than following standards described above, tables which are used primarily for holding raw imports of data extracts from third parties should have their columns match names from the data source.
- CamelCase or other source names that don't adhere to basic Postgres schema object naming requirements should be mapped to Postgres-standard names (i.e. all lowercase, with underscores between terms), but not otherwise changed.
- When converting from non-standard names, divisions between terms that should be separated by underscores are not always automatic. While we can generally assume every capital letter is a new term, that is not always the case. Instead of imposing a fixed standard here, we can decide these matters on a case-by-case basis, at the data engineer's discretion.

## Index names

- Index names should remain as the default name generated by Postgres when creating indices.

## Constraint names

- Constraint names should also follow Postgres default naming.

## View names

- For performance and other considerations, views are sometimes replaced with materialized views, or even with ordinary physical tables. The database catalog includes enough metadata to determine if an object is a table or a view. Views should not use any special prefixes or suffixes, and should be named as any other table would be.

## Representation term abbreviations

Note: any term listed on the Logical model document that doesn't appear below should just be spelled out in names.

Abbr    | Term
------- | ---------------------
`amt`   | `amount`
`cd`    | `code`
`cnt`   | `count`
`dt`    | `datetime`
`descr` | `description`
`id`    | `identifier`
`mtr`   | `metric` or `measure`
`hgt`   | `height`
`wgt`   | `weight`
`nbr`   | `number`
`pct`   | `percent`
`rt`    | `rate`
`seq`   | `sequence`
`txt`   | `text`

## Other abbreviations

- _Please add here as needed for class or property words that need abbreviations to keep name lengths reasonable. As these get added, the same abbreviation should always be used for the same term._

Abbr   | Term
------ | --------------
`assn` | `association`
`org`  | `organization`
`spec` | `specification`

## Credits

Portions of this material adapted from the ISO 11179 Metadata Registries standard, the Data Modeling Standards of the Object Management Group, and _SQL Programming Style_ by Joe Celko.
