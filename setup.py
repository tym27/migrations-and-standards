from setuptools import setup

setup(
    name='migrations_and_standards',
    version='0.0.1.dev1',
    package_dir={'migrations_and_standards': ''},
    packages=['migrations_and_standards'],
    scripts=[
        'make_current.py',
    ],
    py_modules=[
        'make_current',
    ],
    entry_points={'console_scripts': ['make_current=make_current:main']},
    description='Database schema migration for OAO',
    author='~tym',
    author_email='thomas.yager-madden@adops.com',
    install_requires=[
        'python-dotenv',
        'sh',
    ],
    classifiers=[
        'Development Status :: 5 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries',
        'Topic :: Utilities',
    ],
    url='https://gitlab.com/tym27/migrations-and-standards.git',
)
