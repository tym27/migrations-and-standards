# Best practice Postgres connections in Kubernetes Pods

_\[Note: using 'Pod' here as shorthand for any resource that ultimately creates a Pod — i.e. RCs, Jobs, Deployments, &c.\]_



## TL;DR

1. Database connection in URL form, stored in a ConfigMap
    1. Database URLs will have pgBouncer service `schubert-pgbouncer` as the hostname
2. Postgres password in a Secret, used to set $PGPASSWORD in the environment
3. There is no step 3 🙂
4. (Unless you need more than one database connection, in which case see below.)
4. ~~Include a gcloud_sql_proxy image in your Pod as a sidecar container to handle connection to your Cloud SQL instance~~ (Deprecated in favor of connection via pgBouncer services, which handle the `cloud_sql_proxy` business under the covers.)

## Connection Details

- Define all database parameters in a single URL for each connection your app will need. Most ORMs and connection libraries include this option out-of-the-box; if yours doesn't, please see @tym to talk about other options. If necessary, it's better to write a function in your app to parse a database URL than to use separate configuration keys for each connection element.

  A Postgres database URL has the general form `postgres://user@hostname:port/database_name`. For OAO services and applications hosted on GKE clusters, the only hostname for pgBouncer connections right now is `schubert-pgbouncer` (this service runs in the `default` namespace on all our k8s clusters). For testing, SQLite URLs are like `sqlite:///path/to/foo.db`.

- Provision the database URL as a ConfigMap in the cluster (and namespace) you will deploy your Pod to. Easiest way to do this is with the  `--from-literal` flag:

  ```bash
  kubectl create configmap <db-map-name> \
  --from-literal=dburl=postgres://<user>@schubert-pgbouncer:6432/<database_name>
  ```

- Provision the database password as a Secret:

  ```bash
  kubectl create secret generic <db-secret-name> \
  --from-literal=pg-pass=<actual-secret-password>
  ```
- Note that with `$PGPASSWORD` set in the environment, it shouldn't be necessary to include a password in your connection string.

- Now you can use the database URL ConfigMap and password secret to define environment variables in your Kubernetes resource manifest, like so:

  ```YAML
  spec:
    containers:
    - name: <app-name>
      image: us.gcr.io/lexical-cider-93918/<app-name>:<build>
      env:
      - name: DBURL
        valueFrom:
          configMapKeyRef:
            name: <db-map-name>
            key: dburl
      - name: PGPASSWORD
        valueFrom:
          secretKeyRef:
            name: <db-secret-name>
            key: pg-pass
  ```

## Apps that connect to more than one database

Certain pods need to connect to more than one database, and/or use more than account and password to do so. In that case, you will need to alter the instructions above slightly:

1. Create unique keys for each database URL in your ConfigMap, which you can assign to distinct envars in your pod that your app can reference as needed.

  ```bash
  kubectl create configmap <db-map-name> \
  --from-literal=dburl_a=postgres://<user>@schubert-pgbouncer:6432/<database_name_a> \
  --from-literal=dburl_b=postgres://<user>@schubert-pgbouncer:6432/<database_name_b>
  ```

2. Instead of a single password secret assigned to an envar, you can create a `.pgpass` file with a line for each connection as [described in the Postgres documentation](https://www.postgresql.org/docs/current/static/libpq-pgpass.html). Use `--from-file` flag to create a k8s Secret from this file, which you can mount to your pod. Be sure to set `PGPASSFILE` in the environment with the directory path to this mount, so that libpq knows where to look for the file.

    ```bash
    kubectl create secret generic <db-secret-name> \
    --from-file=pgpass=</path/to/.pgpass>
    ```

  Resulting k8s manifest would look like this example:

    ```YAML
    spec:
      containers:
      - name: <app-name>
        image: us.gcr.io/lexical-cider-93918/<app-name>:<build>
        env:
        - name: DBURL_A
          valueFrom:
            configMapKeyRef:
              name: <db-map-name>
              key: dburl_a
        - name: DBURL_B
          valueFrom:
            configMapKeyRef:
              name: <db-map-name>
              key: dburl_b
        - name: PGPASSFILE
          value: '/secret/pgpass'
        volumeMounts:
          - name: pgpass-file
            mountPath: /secret
      volumes:
        - name: pgpass-file
          secret:
            secretName: <db-secret-name>
    ```
