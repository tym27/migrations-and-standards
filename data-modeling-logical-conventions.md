# Data Architecture: Logical data modeling conventions

Submitted by Thomas Yager-Madden, Data Architect

## Overview

This document provides a set of conventions and guidelines that apply to logical data models. Inasmuch as a logical data model represents the perspective of the creators and users of the data, conventions and guidelines are more appropriate than standards. The primary aim of any such model is to provide a basis for communication and discussion before any physical implementation design.

The logical data model is implementation independent, and should represent the complete information requirement for the universe of discourse under description, from the users' perspective. It should contain only atomic data: data at the lowest granularity from the operational source, which is not redundant, and not derived from other data.

To aid rapid application development, Data Architecture requests to take part in the development process from the earliest stages of any project. This allows suggestions in the use of existing structures, incorporating database standards in the initial design, and eliminating unnecessary rework to retrofit standards.

## Principles

- All data models should be as self-documenting as practicable.
  - In a logical model, data element names should not be abbreviated.
  - Metadata should be accessible with same tools used to access data
- A successful model should aim for balance between abstraction and intelligibility (more abstract models are more flexible, but tend to be more difficult to code against).
- Transaction-Processing (OLTP) databases should use Third Normal Form (3NF).
  - Further normalization is not typically necessary.
  - Although we may wish to denormalize some data for performance reasons, we should save this as an exercise for physical modeling.
- Analytic (OLAP) databases should use the star schema pattern of dimensional design, with dimension attributes and hierarchies flattened to First Normal Form (1NF), joined to fact tables in 3NF.
  - While we shouldn't rule out occasional "snowflake" normalization of dimension data, in practice this is rarely actually helpful, and we should discourage this pattern.

## Common naming guidelines

- Spell out all terms in any data element name fully, using no abbreviations
  - Certain acronyms are more widely known than their expansions; these may be exceptions to the no abbreviations rule. We should maintain a list of approved acronyms as an appendix to this document.
  - Abbreviations may be applied in a resulting physical model, using a standard mapping.
- All names should use alphabetic and numeric characters, no punctuation marks or special characters.
- Names should reflect the business nature of the named data element.
- Terminology used in names should be subject to review with subject matter experts and/or derived from business documentation or references.
- Data element names should express a single idea or concept that is clear and self-explanatory.
- Names should not include possessive forms, articles, conjunctions, verbs, or prepositions.
- Avoid using corporate or brand names, project code names, application or report names in data element names.

## Entities

An entity is the representation of a distinguishable person, place, thing, concept, event or state which has characteristics, properties, or relationships which are of interest in the domain of discourse belonging to a given project or application.

- Although we can isolate physical data by database and schema or namespace, even so it helps for entity names to be unique across database systems.
- An entity name expresses one concept or idea – each entity should have a unique meaning.
- Do not depend on or refer to other data elements or concepts in any entity name.
- An entity name should be a noun or noun phrase in the singular form.
- The entity name should describe what the thing is, rather than _what it is not, or how it is used_.
- Entity names should describe business meaning in terms familiar to users.
- Names should be concise, but adequately descriptive. Brevity for the sake of saving keystrokes alone is of no value. More expressive names are better.
- Formal code lookup tables should include the term 'reference' to distinguish them from fundamental entities.
- Associative or "cross join" entities used to resolve many-to-many relationships should have a meaningful business name, instead of a bare concatenation of the parent entity names. Appending the term 'association' is a good enough generic way to do this if nothing more specific applies.

### Keys

A key identifies specific occurrences of an entity. They can be simple, consisting of a single attribute, or they can be composite, consisting of two or more attributes.

- Every entity of interest to the business should have a natural or candidate key that uniquely identifies occurrences of the entity. Ideally, this should be an industry standard abbreviation or coding scheme. Where such a coding system does not exist, we should consider inventing one.
- Besides recognizing the business key for each entity, we should generate a serial surrogate integer key to use as the formal primary key in the data model. The surrogate key shall have the class name of the entity appended with the representation term 'identifier'. In the resulting physical model, this will simplify join syntax and improve performance.
- Foreign keys should have the same name as their parent key whenever possible.
  - The obvious exception applies to entities with more than one foreign key inherited from the same parent entity (e.g. more than one significant dates tied back to a date dimension). In this case, add a modifier to each foreign key with the role it plays in describing the child entity.

## Attributes

- Attribute definitions should communicate the essential business nature and purpose of the attribute.
- An attribute name has an object class, a property, and a representation term.
- An object class represents an idea, abstraction, or thing in the real world, such as an `ad unit` or `publisher`.
- A property is something that describes all objects in the class, such as `creative size` or `identifier`.
- A representation term expresses the set or domain of allowed values for the element, such as `amount`, `count`, or `name`.

### Representation terms

`amount, price, cost`: A quantity of money; 'price' is set before a transaction, 'cost' indicates a transacted expense  
`code`: An abbreviation or identifying string or number used in a formal vocabulary or other external reference  
`count`: A number that can be arrived at by counting, as events, instances, objects, etc
`date, datetime, time`: Chronology  
`description`: A more verbose or detailed statement that represents something in words  
`flag`: A boolean  
`identifier`: Reserved for serial surrogate keys in the immediate system being modeled. Identifier values imported from systems outside our immediate control should use 'code' instead.  
`measure` (or `height`, `weight`, etc): A number arrived at by comparison to a scale, rather than by counting  
`name`: A word or words by which a thing is designated and distinguished from others  
`number`: Certain 'code' values (like Social Security Numbers) are formed with digits and more commonly called 'number'. We should stick with common usage in these cases.  
`percent`: A numeric portion out of 100 units  
`rate`: A number which represents a proportion or ratio  
`sequence`: An ordinal number  
`text`: An unconstrained, freeform comment or notes field  
`unit`: Designates the atomic scale used in a given measure  

## Model diagram layout

- We recommend Information Engineering (IE or"Crow’s Feet") notation, which is more familiar and easier for users to interpret than other notations.
- As much as possible while maintaining consistency with other guidelines, keep the _many_ (or "crow's foot") end of relationship lines pointing down or to the right. This puts fundamental entities in the top left area of the diagram, and associative and subtype entities in the lower or rightward areas—similar to natural English reading.
- Keep relationship lines as straight as possible. Avoid unnecessary bends. Too many symbols clutter the diagram and make it confusing to the viewer.
- Avoid crossing relationship lines. Crossed lines make it difficult to understand which entities are related.
- Arrange dimensional models in the familiar "star schema" pattern, with fact tables surrounded by related dimension tables.

## Credits

Portions of this material adapted from the ISO 11179 Metadata Registries standard, the Data Modeling Standards of the Object Management Group, and _SQL Programming Style_ by Joe Celko.
