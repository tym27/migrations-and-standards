#!/bin/bash

# =======
# Quick local dev database from schema
# Assumes you have docker daemon running on your machine
#   and that this repo's main directory is your PWD.
# -------
# Invoking this script with a database name from the repo will start
# a postgres:10-alpine container with one database with that name.
# Next it will pass the current schema definition to build the tables
# And drop you into a psql command prompt as the superuser in that database.
# -------
# Author: tym, 2017-08-14
# =======

# Verify usage
if [ $# -lt 1 ];
    then
        echo 'Usage: Please pass database name'
        exit
fi

#Tear down previous instance if running
# -------
# TODO: add support for a multiple dev containers
#       at once / container name arg
# ======+
docker rm -f dev-postgres &> /dev/null

DATABASE_SCHEMA=$1
SCHEMAPATH="/schemas/$DATABASE_SCHEMA/current/_schema-dump.sql"

# Start the postgres container
docker run --name dev-postgres \
-e POSTGRES_DB=$DATABASE_SCHEMA \
-v $(pwd):/schemas \
-p 5432:5432 \
-d postgres:10-alpine

# Give it some time to initdb
sleep 5
# Create the schema
docker exec dev-postgres su - postgres -c "psql -f $SCHEMAPATH $DATABASE_SCHEMA"
# Start the client
psql -h localhost -p5432 -U postgres $DATABASE_SCHEMA
