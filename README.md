# Schema Migrations and Modeling Standards

This repository keeps DDL definitions of PostgreSQL databases. It also provides also a standard way of using the [`db-migrate`](https://www.npmjs.com/package/db-migrate) node.js database migration tool to track and manage database changes, and an automated process (python script `make_current.py`) for updating the current schema definition files after applying migrations.

In keeping with 12-factor usage, the migration process is designed to act on a $DATABASE_URL set in the environment. Because several databases are managed here, this is set using a `.env` which sets the URL and password for each database. The repository includes `.env.example` files, which should be copied to `.env` and the `PGPASSWORD` value set. Actual `.env` files are excluded by .gitignore and *under no circumstances should be checked in to version control*.

Folder structure for each database:

```plain-text
_ ${database}
    |_ initial
    |_ migrations
    |_ current
    .env  
```

- `initial` provides a starting baseline before any migrations have run. This shouldn't change.
- `migrations` keeps the actual revision scripts.
- `current` gets automatically updated with the latest changes, generated with `pg_dump`, after applying migrations.

The schema directories include a `_schema_dump.sql` file for each database, which you can use to set up a copy of the schema in a new environment. Whether to begin with the `initial` schema and run all the migrations, or start from the `current` schema will vary depending on the situation.

Each directory also includes separate files per database object, split from the main dump file. These again are automatically generated, and are included for convenient reference: they are useful if you want to look up the defintion of a particular table or function, but won't typically be used to build a database from (unless you are interested in testing one particular database object).

## Installation

At least for the time being, while `db-migrate` is a node package, the `make_current` script is python. [TODO: port make-current to JS (or all standard-lib Python).] So you will need a working environment that supports both. The script requires python 3.6, and the instructions that follow assume you have `pipenv` in your environment (although you should be able to run with regular pip or just `setup.py install` as well).

```sh
git clone https://gitlab.com/tym27/migrations-and-standards.git
cd migrations-and-standards
pipenv install --three  # or `pip install -e .` in a python3 virtualenv
pipenv shell  # or `source <virtualenv>/bin/activate`
# db-migrate requires a global install, but will execute from our local module
npm install -g db-migrate && npm install
```

## Adding a migration

- Start a new branch, named for your planned database change. The suggested naming convention is to begin with the SQL statement verb, followed by the object acted upon: e.g. `create-api-user`, `alter-line-item`, `drop-agency`, etc.
- `cd` to the subdirectory for the database you want to change.
- Run the command `cp .env.example .env`
- Edit the `.env` file to include the password for the `migration` user
- Create the migration script with command `db-migrate create my-cool-migration-name`. (Your actual cool migration name should be brief and named similarly to the branch name convention described above.)
- This will create a migration script in the appropriate `${database}/migrations` directory, with your migration name following a timestamp in the file name. It will also add similarly-named `*-up.sql` and `*-down.sql` in `migrations/sqls`.
- Edit the SQL files with your desired changes in standard Postgres-dialect SQL, putting the forward migration in `-up.sql` and the rollback in `-down.sql`.
- Use `IF EXISTS/IF NOT EXISTS` with all `CREATE` or `DROP` statements wherever available, so that both forward and rollback changes will be idempotent when possible.
- For destructive changes, such as dropping a table or column, it is a good idea to set the data aside in a scratch table (named like `z_*`) with `SELECT... INTO` before dropping. Then the rollback script can use `INSERT... SELECT` to restore the data.
- When the SQL files are finished, commit your changes in git.
- You should test all migrations on a dev or test database, running both forward and rollback, before pushing to GitHub. (See "Running a migration" below.)
- Migrations should go through Pull Request review by DBAs and be merged to master before applying to Production.

## Running a migration

To test your migration, edit the `DATABASE_URL` in the local `.env` file to point to your test database. (`dev-database-docker.sh` is included in this repo to provide a convenient way to get a copy of any of the schemas running locally.) Once the `.env` file is set, the following steps apply whether migrating locally or onto the production database:

- For connection to live Cloud SQL instances, run `mkdir -p /tmp/cloudsql && cloud_sql_proxy -dir=/tmp/cloudsql &`
- From the appropriate database directory: `db-migrate up`.
- To roll back the most recently executed migration: `db-migrate down`.
- To roll back all migrations to the initial starting schema: `db-migrate reset`
- To upgrade to a particular revision `db-migrate up <migration name>` (migration name is the revision file names minus the `.js` extension) or `db-migrate up -c *n*` to upgrade _n_ migrations.
- Roll back to a particular revision in a like manner `db-migrate down <name>` or `db-migrate down -c n`
- When the migration is complete, run the command `make_current` from the appropriate `${database}` subdirectory.

For more on `db-migrate` command options, see [db-migrate.readthedocs.io](https://db-migrate.readthedocs.io/en/latest/).
