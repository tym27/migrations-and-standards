# Cloud SQL connection from desktop client

Official GCP documents on Cloud SQL Proxy are [here](https://cloud.google.com/sql/docs/postgres/connect-admin-proxy). This document summarizes the most relevant points for our purposes.

## Getting the proxy

- Download the proxy binary and make it executable:

  ```bash
  wget https://dl.google.com/cloudsql/cloud_sql_proxy.darwin.amd64 \
  -O /usr/local/bin/cloud_sql_proxy
  chmod +x /usr/local/bin/cloud_sql_proxy
  ```

## Connect the proxy

### If you have the Google Cloud SDK installed:

- If you have `gcloud` installed on your machine, make sure you have authenticated to it and set `lexical-cider-93918` as your default project
- With this setup, there is no need to specify instances, the proxy will automatically discover all database instances in the project and create sockets for them, using your gcloud credential to authenticate to the proxy connection.
- Start the proxy in the background, passing a directory name where it can create the sockets:

  ```bash
  mkdir -p -m 755 /tmp/cloudsql
  cloud_sql_proxy -dir=/tmp/cloudsql &
  ```

- Note the instance connection name for the Cloud SQL Postgres instance you want to connect to. The list of instances is [on the cloud console here](https://console.cloud.google.com/sql/instances). If you click through to details on a given instance, you will find the **Instance connection name** listed under Properties. Instance connection names have the general form `project-id:region:instance-id`, so for our setup you can safely assume that any instance connection name will have `lexical-cider-93918:us-central1:` prepended to the instance name. (For example `lexical-cider-93918:us-central1:pg-master`.)
- Now you can start your Postgres client as usual. For your connection, set
  - host=/tmp/cloudsql/lexical-cider-93918:us-central1:<pg-instance>
  - sslmode=disable
  - And your usual database username and password

  We set `sslmode` to `disable` because the proxy provides an encrypted connection.
- 💥

### Without installing the Cloud SDK:

- Get the Cloud SQL Client Credentials document from the 1Password Technology vault and save to your machine.
- Note the instance connection name for the Cloud SQL Postgres instance you want to connect to. The list of instances is [on the cloud console here](https://console.cloud.google.com/sql/instances). If you click through to details on a given instance, you will find the **Instance connection name** listed under Properties. Instance connection names have the general form `project-id:region:instance-id`, so for our setup you can safely assume that any instance connection name will have `lexical-cider-93918:us-central1:` prepended to the instance name. (For example `lexical-cider-93918:us-central1:pg-master`.)
- Start the proxy in the background, passing the instance connection name and credential file as arguments as follows:

  ```bash
  cloud_sql_proxy \
  -instances=lexical-cider-93918:us-central1:<pg-instance>=tcp:5432 \
  -credential_file=<path/to>/creds.json &
  ```

- Now you can start your Postgres client as usual. For your connection, set
  - host=127.0.0.1
  - port=5432
  - sslmode=disable
  - And your usual database username and password

  We set `sslmode` to `disable` because the proxy provides an encrypted connection.
- 💥
